import nltk
from nltk.tokenize import word_tokenize
from nltk.tag import pos_tag
from nltk.chunk import ne_chunk
from nltk.stem import WordNetLemmatizer
import time

# Descarga de recursos necesarios para NLTK (Quitar comentarios si es la primera vez que se usa el código)
# nltk.download('punkt')
# nltk.download('averaged_perceptron_tagger')
# nltk.download('maxent_ne_chunker')
# nltk.download('words')
# nltk.download('wordnet')

def tokenizacion(texto):
    start_time = time.time()  # Registro de tiempo inicial
    tokens = word_tokenize(texto)
    end_time = time.time()  # Registro de tiempo final
    tiempo_ejecucion = end_time - start_time  # Calculando tiempo de ejecución
    return tokens, tiempo_ejecucion

def lematizacion(texto):
    start_time = time.time()  # Registro de tiempo inicial
    lemmatizer = WordNetLemmatizer()
    tokens = word_tokenize(texto)
    tagged_tokens = pos_tag(tokens)
    lemmatized_tokens = []
    for token, tag in tagged_tokens:
        # Convertimos las etiquetas POS de Penn Treebank a etiquetas de WordNet
        tag = tag[0].upper()
        tag = tag if tag in ['A', 'N', 'V'] else None
        try:
            lemmatized_token = lemmatizer.lemmatize(token, pos=tag) if tag else lemmatizer.lemmatize(token)
        except KeyError:
            # Si no se encuentra la etiqueta POS en WordNet, lematizamos sin especificar la etiqueta
            lemmatized_token = lemmatizer.lemmatize(token)
        lemmatized_tokens.append(lemmatized_token)
    end_time = time.time()  # Registro de tiempo final
    tiempo_ejecucion = end_time - start_time  # Calculando tiempo de ejecución
    return lemmatized_tokens, tiempo_ejecucion

def etiquetado_pos(texto):
    start_time = time.time()  # Registro de tiempo inicial
    tokens = word_tokenize(texto)
    etiquetado = pos_tag(tokens)
    end_time = time.time()  # Registro de tiempo final
    tiempo_ejecucion = end_time - start_time  # Calculando tiempo de ejecución
    return etiquetado, tiempo_ejecucion

def reconocimiento_entidades(texto):
    start_time = time.time()  # Registro de tiempo inicial
    tokens = word_tokenize(texto)
    etiquetado = pos_tag(tokens)
    entidades = ne_chunk(etiquetado)
    end_time = time.time()  # Registro de tiempo final
    tiempo_ejecucion = end_time - start_time  # Calculando tiempo de ejecución

    # Lista para almacenar las entidades nombradas encontradas
    entidades_nombradas = []

    # Iterar sobre las entidades y extraer las entidades nombradas
    for subarbol in entidades:
        if isinstance(subarbol, nltk.tree.Tree):
            if subarbol.label() == 'PERSON' or subarbol.label() == 'ORGANIZATION' or subarbol.label() == 'GPE':
                entidad_nombrada = " ".join([palabra for palabra, etiqueta in subarbol.leaves()])
                entidades_nombradas.append((entidad_nombrada, subarbol.label()))

    # Formatear las entidades reconocidas
    entidades_reconocidas = [entidad.upper() for entidad, etiqueta in entidades_nombradas]
    entidades_reconocidas_str = ", ".join(entidades_reconocidas)

    return entidades_nombradas, tiempo_ejecucion, entidades_reconocidas_str

def menu():
    print("1. Tokenización")
    print("2. Lematización")
    print("3. Etiquetado de partes del discurso")
    print("4. Reconocimiento de entidades nombradas")
    print("5. Salir")

def main():
    while True:
        menu()
        opcion = input("Seleccione una funcionalidad (1-5): ")
        
        if opcion == '2':
            texto = input("Ingrese el texto en inglés: ")
            lematizado, tiempo_ejecucion = lematizacion(texto)
            print("Lematización:", lematizado)
            print("Tiempo de ejecución:", tiempo_ejecucion, "segundos")
        elif opcion == '1':
            texto = input("Ingrese el texto en inglés: ")
            tokens, tiempo_ejecucion = tokenizacion(texto)
            print("Tokenización:", tokens)
            print("Tiempo de ejecución:", tiempo_ejecucion, "segundos")
        elif opcion == '3':
            texto = input("Ingrese el texto en inglés: ")
            etiquetado, tiempo_ejecucion = etiquetado_pos(texto)
            print("Etiquetado de partes del discurso:", etiquetado)
            print("Tiempo de ejecución:", tiempo_ejecucion, "segundos")
        elif opcion == '4':
            texto = input("Ingrese el texto en inglés: ")
            entidades_nombradas, tiempo_ejecucion, entidades_reconocidas = reconocimiento_entidades(texto)
            print("Reconocimiento de entidades nombradas:")
            for entidad, etiqueta in entidades_nombradas:
                print(f"{entidad}: {etiqueta}")
            print("Tiempo de ejecución:", tiempo_ejecucion, "segundos")
            print("Entidades reconocidas:", entidades_reconocidas)
        elif opcion == '5':
            print("¡Hasta luego!")
            break
        else:
            print("Opción no válida. Por favor, seleccione una opción válida.")

if __name__ == "__main__":
    main()
