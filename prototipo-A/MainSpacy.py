import spacy
import time
import spacy.cli

# Descarga de recursos necesarios para SpaCy(Quitar comentario en primera ejecución)
# spacy.cli.download("es_core_news_sm")

# Inicialización de Spacy para español
nlp = spacy.load("es_core_news_sm")

def tokenizacion(texto):
    start_time = time.time()  # Registro de tiempo inicial
    doc = nlp(texto)
    tokens = [token.text for token in doc]
    end_time = time.time()  # Registro de tiempo final
    tiempo_ejecucion = end_time - start_time  # Calculando tiempo de ejecución
    return tokens, tiempo_ejecucion

def etiquetado_pos(texto):
    start_time = time.time()  # Registro de tiempo inicial
    doc = nlp(texto)
    etiquetado = [(token.text, token.pos_) for token in doc]
    end_time = time.time()  # Registro de tiempo final
    tiempo_ejecucion = end_time - start_time  # Calculando tiempo de ejecución
    return etiquetado, tiempo_ejecucion

def lematizacion(texto):
    start_time = time.time()  # Registro de tiempo inicial
    doc = nlp(texto)
    lematizados = [token.lemma_ for token in doc]
    end_time = time.time()  # Registro de tiempo final
    tiempo_ejecucion = end_time - start_time  # Calculando tiempo de ejecución
    return lematizados, tiempo_ejecucion

def reconocimiento_entidades(texto):
    start_time = time.time()  # Registro de tiempo inicial
    doc = nlp(texto)
    entidades = [(ent.text, ent.label_) for ent in doc.ents]
    end_time = time.time()  # Registro de tiempo final
    tiempo_ejecucion = end_time - start_time  # Calculando tiempo de ejecución
    return entidades, tiempo_ejecucion

def menu():
    print("1. Tokenización")
    print("2. Etiquetado de partes del discurso")
    print("3. Lematización")
    print("4. Reconocimiento de entidades nombradas")
    print("5. Salir")

def main():
    while True:
        menu()
        opcion = input("Seleccione una funcionalidad (1-5): ")
        
        if opcion == '1':
            texto = input("Ingrese el texto en español: ")
            tokens, tiempo_ejecucion = tokenizacion(texto)
            print("Tokens:", tokens)
            print("Tiempo de ejecución:", tiempo_ejecucion, "segundos")
        elif opcion == '2':
            texto = input("Ingrese el texto en español: ")
            etiquetado, tiempo_ejecucion = etiquetado_pos(texto)
            print("Etiquetado de partes del discurso:", etiquetado)
            print("Tiempo de ejecución:", tiempo_ejecucion, "segundos")
        elif opcion == '3':
            texto = input("Ingrese el texto en español: ")
            lematizados, tiempo_ejecucion = lematizacion(texto)
            print("Lematización:", lematizados)
            print("Tiempo de ejecución:", tiempo_ejecucion, "segundos")
        elif opcion == '4':
            texto = input("Ingrese el texto en español: ")
            entidades, tiempo_ejecucion = reconocimiento_entidades(texto)
            print("Reconocimiento de entidades nombradas:", entidades)
            print("Tiempo de ejecución:", tiempo_ejecucion, "segundos")
        elif opcion == '5':
            print("¡Hasta luego!")
            break
        else:
            print("Opción no válida. Por favor, seleccione una opción válida.")

if __name__ == "__main__":
    main()
